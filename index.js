var express = require('express')
var bodyParser = require('body-parser')
var axios = require('axios')
const path = require('path')
const apiKey = process.env.FORM_KEY
const postEndpoint = process.env.POST_ENDPOINT
const emailEndpoint = process.env.EMAIL_ENDPOINT

var app = express()

app.use(bodyParser.json())
app.enable('trust proxy')
app.use('/', express.static(path.join(__dirname, 'build')))

app.post('/send', async function (req, res) {
  await axios({
    method: 'POST',
    url: postEndpoint,
    headers: {
      'Accept': 'application/json',
      'X-DreamFactory-Api-Key': apiKey,
      'Content-Type': 'application/json'
    },
    data: {
      resource: [
        {
          name: req.body.name,
          email: req.body.email,
          description: req.body.description,
          other_explanation: req.body.other_explanation,
          course: req.body.course,
          librarian: req.body.librarian,
          primary_workshop: req.body.primary_workshop,
          secondary_workshop: req.body.secondary_workshop,
          process: req.body.process,
          participants: req.body.participants,
          primary_duration: req.body.primary_duration,
          alternate_duration: req.body.alternate_duration,
          primary_date_time: req.body.primary_date_time,
          alternate_date_time: req.body.alternate_date_time,
          terms: req.body.terms,
          status: 'New Request'
        }
      ]
    }
  }).then((response) => {
    console.log(response.status)
    if (response.status === 200) {
      res.sendStatus(200)
    } else {
      res.sendStatus(500)
    }
  }).catch((error) => {
    console.log(error)
    res.sendStatus(500)
  })
})

app.post('/email', async function (req, res) {
  await axios({
    method: 'POST',
    url: emailEndpoint,
    headers: {
      'Accept': 'application/json',
      'X-DreamFactory-Api-Key': apiKey,
      'Content-Type': 'application/json'
    },
    data: {
      'template': 'New Instruction Request Notification',
      'instructor_name': req.body.name,
      'course': req.body.course,
      'description': req.body.description,
      'dates': req.body.dates
    }
  }).then((response) => {
    console.log(response.status)
    if (response.status === 200) {
      res.sendStatus(200)
    } else {
      res.sendStatus(500)
    }
  }).catch((error) => {
    console.log(error)
    res.sendStatus(500)
  })
})

app.listen(3000, () => console.log('Server running on port 3000'))
